import _ from 'lodash';
import cytoscape from 'cytoscape';
import nodeHtmlLabel from 'cytoscape-node-html-label';

nodeHtmlLabel(cytoscape); // register extension

// Small helper function for selecting element by id
const id = id => document.getElementById(id);

const location = {
    'hostname': 'localhost',
    'port': 7070
}

// Establish the WebSocket connection and set up event handlers
const ws = new WebSocket("ws://" + location.hostname + ":" + location.port + "/chat");
ws.onmessage = msg => updateChat(msg);
ws.onclose = () => alert("WebSocket connection closed");

// Add event listeners to button and input field
id("send").addEventListener("click", () => sendAndClear(id("message").value));
id("message").addEventListener("keypress", function (e) {
    if (e.keyCode === 13) { // Send message if enter is pressed in input field
        sendAndClear(e.target.value);
    }
});

function sendAndClear(message) {
    if (message !== "") {
        ws.send(message);
        id("message").value = "";
    }
}

function updateChat(msg) { // Update chat-panel and list of connected users
    const data = JSON.parse(msg.data);
    console.log()
}

fetch("http://" + location.hostname + ":" + location.port + "/chat/gsearch?tag=hello")
    .then(response => response.json())
    .then(data => console.log(data));

function component() {
  const element = document.createElement('div');

  // Lodash, currently included via a script, is required for this line to work
  element.innerHTML = _.join(['Hello', 'TS'], ' ');

  return element;
}

document.addEventListener('DOMContentLoaded', function() {
  window.cy = cytoscape({
    container: document.getElementById('cy'), // container to render in
    elements: {
      nodes: [
        { data: { id: 'a', name: 'user1', message: "let's discuss something" } },
        { data: { id: 'b', name: 'user2', message: "yeah, why not... wdyt, where Alien from my fav movie 'Alien' came from?" } },
        { data: { id: 'c', name: 'user3', message: "wazuuuuup" } }
      ],
      edges: [
        { data: { id: 'ab', source: 'a', target: 'b' } },
        { data: { id: 'ac', source: 'a', target: 'c' } }
      ]
    },

    style: [ // the stylesheet for the graph
      {
        selector: 'node',
        style: {
          'shape': 'pentagon',
          'background-color': '#666',
          'label': 'data(name)'
        }
      },

      {
        selector: 'edge',
        style: {
          'width': 3,
          'line-color': '#ccc',
          'target-arrow-color': '#ccc',
          'target-arrow-shape': 'triangle',
          'curve-style': 'bezier'
        }
      }
    ],

    layout: {
      name: 'random',
      fit: true, // whether to fit to viewport
      padding: 30
    }
  });
  window.cy.nodeHtmlLabel([
    {
      query: 'node', // cytoscape query selector
      halign: 'center', // title vertical position. Can be 'left',''center, 'right'
      valign: 'center', // title vertical position. Can be 'top',''center, 'bottom'
      halignBox: 'center', // title vertical position. Can be 'left',''center, 'right'
      valignBox: 'center', // title relative box vertical position. Can be 'top',''center, 'bottom'
      cssClass: '', // any classes will be as attribute of <div> container for every title
      tpl(data) {
        return '<span>from ' + data.name + ': </span></br><span>' + data.message + '</span></br><button>reply</button>'; // your html template here
      }
    }
  ], {
    enablePointerEvents: true
  });
});

//document.body.appendChild(component());

//document.body.appendChild(graphComponent());
